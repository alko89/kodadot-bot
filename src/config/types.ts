/**
 * List of LevelDB keys
 */
export enum DbKeys {
  LAST_SYNC = 'lastSync',
  TELEGRAM_CHAT_ID = 'telegramChatId',
  DISCORD_CHANNEL_ID = 'discordChannelId'
}
