import axios from 'axios';
import { schedule } from 'node-cron';
import level from 'level';
import * as env from './config/env';
import { Telegram } from './lib/telegram';
import { DbKeys } from './config/types';
import { Discord } from './lib/discord';
import { Subquery } from './lib/subquery';
import { Twitter } from './lib/twitter';

const db = level('kodadot');

const telegramBot = new Telegram(env, db);
telegramBot.launch();

const discordBot = new Discord(env, db);
discordBot.login();

const twitterBot = new Twitter(env);

const subquery = new Subquery(env);

schedule(env.cronSchedule, async () => {
  const nextSync = new Date();
  nextSync.setMilliseconds(0);

  db.get(DbKeys.LAST_SYNC, async (err, lastSync) => {
    if (err) { lastSync = new Date().toISOString(); }

    subquery.getNftQueries(lastSync).then(async (data) => {
      console.log(`Total: ${data.nFTEntities.totalCount}`);
      console.log(`SyncTime: ${lastSync}`);
      db.put(DbKeys.LAST_SYNC, nextSync.toISOString());

      for (const node of data.nFTEntities.nodes) {
        const message = `${node.name}\n` +
          `${node.createdAt}\n` +
          `https://nft.kodadot.xyz/rmrk/detail/${node.id}\n`
        ;

        telegramBot.sendMessage(message);
        discordBot.send(message);
        twitterBot.send(message);
      }
    }).catch((e) => {
      console.log(e);
    });
  });
});
