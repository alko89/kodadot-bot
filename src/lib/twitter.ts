import { TwitterClient } from 'twitter-api-client';
import { Env } from '../config/env';

/**
 * Twitter class.
 */
export class Twitter {
  public env: Env;
  public client: TwitterClient;

  /**
   * Class constructor.
   * @param env Environment variables.
   */
  public constructor(env: Env) {
    this.env = env;
    this.client = new TwitterClient({
      apiKey: env.twitterApiKey,
      apiSecret: env.twitterApiSecret,
      accessToken: env.twitterAccessToken,
      accessTokenSecret: env.twitterAccessSecret
    });
  }

  /**
   * Sends a new tweet
   */
  public async send(message: string): Promise<void> {
    await this.client.tweets.statusesUpdate({
      status: message
    });
  }
}
