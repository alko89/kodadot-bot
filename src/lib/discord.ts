import { LevelDB } from 'level';
import { Client, TextChannel } from 'discord.js';
import { Env } from '../config/env';
import { DbKeys } from '../config/types';

/**
 * Discord class.
 */
export class Discord {
  public env: Env;
  public db: LevelDB;
  public client: Client;
  prefix = '/kd';

  /**
   * Class constructor.
   * @param env Environment variables.
   */
  public constructor(env: Env, db: LevelDB) {
    this.env = env;
    this.db = db;
    this.client = new Client();
  }

  /**
   * Login to discord.
   */
  public async login(): Promise<Discord> {
    this.commands();
    await this.client.login(this.env.discordToken);
    return this;
  }

  /**
   * Sends a message to all chats
   */
  public async send(message: string): Promise<void> {
    this.db.get(DbKeys.DISCORD_CHANNEL_ID, async (err, data) => {
      if (err) return;

      const discordChannelIds = JSON.parse(data);
      for (const id of discordChannelIds) {
        const discordChannel = await this.client.channels.cache.get(id) as TextChannel;
        await discordChannel.send(message);
      }
    });
  }

  /**
   * Initiates Bot commands
   */
  private commands(): void {
    this.client.on('message', (message) => {
      if (!message.content.startsWith(this.prefix) || message.author.bot) return;

      const args = message.content.slice(this.prefix.length).trim().split(/ +/);
      const command = (args.shift() as string).toLowerCase();

      switch (command) {
      case 'start': {
        this.db.get(DbKeys.DISCORD_CHANNEL_ID, async (err, data) => {
          let discordChatIds: string[] = [];
          if (err) discordChatIds = [];
          else discordChatIds = JSON.parse(data);

          if (!discordChatIds.find((e) => e == message.channel.id))
            discordChatIds.push(message.channel.id);
          this.db.put(DbKeys.DISCORD_CHANNEL_ID, JSON.stringify(discordChatIds));
        });
        message.channel.send('Hey!');
        break;
      }
      case 'stop': {
        this.db.get(DbKeys.DISCORD_CHANNEL_ID, async (err, data) => {
          let discordChatIds: string[] = [];
          if (err) return;
          discordChatIds = JSON.parse(data);

          discordChatIds = discordChatIds.filter((e) => e != message.channel.id);
          this.db.put(DbKeys.DISCORD_CHANNEL_ID, JSON.stringify(discordChatIds));
        });
        message.channel.send('Bye!');
        break;
      }
      }
    });
  }
}
