import { gql, GraphQLClient } from 'graphql-request';
import { Env } from '../config/env';

const NFT_ENTITIES = gql `
query nFTEntities($date: Datetime!) {
  nFTEntities (orderBy: CREATED_AT_ASC, filter: { 
    	createdAt: {
        greaterThan: $date
      },
      name: {
        notLike: "%Kanaria%"
      }
    }) {
      totalCount
      nodes {
        id
        name
        instance
        transferable
        collectionId
        metadata
        createdAt
      }
  }
}
`;

/**
 * Subquery class.
 */
export class Subquery {
  public env: Env;
  public client: GraphQLClient;

  /**
   * Class constructor.
   * @param env Environment variables.
   */
  public constructor(env: Env) {
    this.env = env;
    this.client = new GraphQLClient(env.subqueryUrl as string);
  }

  public getNftQueries(from: Date): Promise<any> {
    return this.client.request(NFT_ENTITIES, { date: from });
  }
}
